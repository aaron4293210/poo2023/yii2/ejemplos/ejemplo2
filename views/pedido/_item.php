<?php
    use yii\helpers\Html;
?>

<div class="card text-center">
    <div class="card-header bg-dark text-white">
        <div class="card-title">
            <h5 class="card-title">
                <?= 
                    Html::a("Pedido {$model->id}", 
                        ['view', 'id' => $model->id], 
                        ['class' => 'link-info link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover text-decoration-none text-white'], 
                    )
                ?>
            </h5>
        </div>
    </div>

    <div class="card-body">
        <p class="card-text lead text-muted"><?= $model->cliente->nombre ?> <?= $model->cliente->apellido1 ?> <?= $model->cliente->apellido2 ?></p>

        <?= 
            Html::ul($model, [
                'class' => 'list-group list-group-flush', 
                "item" => function ($valor, $indice) {
                    if (!in_array($indice, ['id']) && $valor) {
                        return "<li class='list-group-item list-group-item-action' data-bs-toggle='tooltip' title='{$indice}'>{$valor}</li>";
                    }
                }
            ]) 
        ?>
    </div>
</div>