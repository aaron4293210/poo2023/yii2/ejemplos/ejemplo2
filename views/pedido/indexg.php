<?php
    use app\models\Pedido;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\grid\ActionColumn;
    use yii\grid\GridView;

    /** @var yii\web\View $this */
    /** @var yii\data\ActiveDataProvider $dataProvider */

    $this->title = 'Administracion Pedidos';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="pedido-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('+', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'total',
            'fecha',
            'id_cliente',
            // Opcion 1
            //'cliente.nombre', coloco el nombre de la relacion (esta en el getter del modelo).(nombre del campo a mostrar)
            // Opcion 2
            [
                'label' => 'Nombre del cliente',
                'value' => 'cliente.nombre'
            ],
            'id_comercial',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Pedido $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>
</div>