<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    /** @var yii\web\View $this */
    /** @var app\models\Pedido $model */

    $this->title = "Pedido {$model->id}";
    $this->params['breadcrumbs'][] = ['label' => 'Pedidos', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
    \yii\web\YiiAsset::register($this);
?>

<div class="pedido-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Acutalizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Esta seguro de que quiere borrar este pedido?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'total',
            'fecha',
            'id_cliente',
            [
                'label' => 'Nombre Cliente Completo',
                'value' => function ($model) {
                    return "{$model->cliente->nombre} {$model->cliente->apellido1} {$model->cliente->apellido2}";
                }
            ],
            'id_comercial',
            'comercial.nombre',
        ],
    ]) ?>
</div>
