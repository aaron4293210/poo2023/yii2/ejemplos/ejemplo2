<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    /** @var yii\web\View $this */
    /** @var app\models\Cliente $model */

    $this->title = "ID: {$model->id}";
    $this->params['breadcrumbs'][] = ['label' => 'Clientes', 'url' => ['index']];
    $this->params['breadcrumbs'][] = "$this->title";
    \yii\web\YiiAsset::register($this);
?>

<div class="cliente-view">
    <h1>Modificando el cliente con <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => "¿Esta seguro de que quiere borrar al cliente con ID: {$model->id}?",
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            'apellido1',
            'apellido2',
            'ciudad',
            'categoría',
        ],
    ]) ?>
</div>