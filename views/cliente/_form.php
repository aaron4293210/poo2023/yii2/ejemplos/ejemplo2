<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /** @var yii\web\View $this */
    /** @var app\models\Cliente $model */
    /** @var yii\widgets\ActiveForm $form */
?>

<div class="cliente-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'apellido1')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'apellido2')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'ciudad')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'categoría')->textInput() ?>
        <?= $form->field($model, 'categoría')->dropDownList(
            [
                '1' => 'Premium',
                '2' => 'Estandar',
                '3' => 'Basico',
            ],[
                'prompt' => 'Selecciona una categoría',
            ]) 
        ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>