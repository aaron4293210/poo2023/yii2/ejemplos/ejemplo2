<div class="card">
    <div class="card-body">
        <h5 class="card-title">Comercial <?= $model->id ?></h5>
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item list-group-item-action">Nombre: <?= $model->nombre ?></li>
        <li class="list-group-item list-group-item-action">Apellido1: <?= $model->apellido1 ?></li>
        <li class="list-group-item list-group-item-action">Apellido2: <?= $model->apellido2 ?></li>
        <li class="list-group-item list-group-item-action">Ciudad: <?= $model->ciudad ?></li>
        <li class="list-group-item list-group-item-action">Categoría: <?= $model->categoría ?></li>
    </ul>
</div>