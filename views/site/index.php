<?php
    /** @var yii\web\View $this */

use yii\helpers\Html;

    $this->title = 'Gestion de Pedidos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gesion del Almacen</h1>

        <p class="lead">Gestion de Comerciales, Clientes y Pedidos</p>
    </div>

    <div class="body-content">
        <div class="row text-center">
            <div class="col-lg-4 mb-3 p-4 border rounded">
                <h2>Clientes</h2>
                <p>
                    <?= Html::a('Ver Clientes', ['/cliente'], 
                        ['class' => 'btn btn-warning']) 
                    ?>
                </p>
            </div>

            <div class="col-lg-4 mb-3 p-4 border rounded">
                <h2>Comerciales</h2>
                <p>
                    <?= Html::a('Ver Comerciales', ['/comercial'], 
                        ['class' => 'btn btn-warning']) 
                    ?>
                </p>
            </div>

            <div class="col-lg-4 mb-3 p-4 border rounded">
                <h2>Pedidos</h2>
                <p>
                    <?= Html::a('Ver Pedidos', ['/pedido'], 
                        ['class' => 'btn btn-warning']) 
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>
