<?php
    namespace app\models;

    use Yii;
    use yii\helpers\ArrayHelper;

    /**
     * This is the model class for table "pedido".
     *
     * @property int $id
     * @property float $total
     * @property string|null $fecha
     * @property int $id_cliente
     * @property int $id_comercial
     *
     * @property Cliente $cliente
     * @property Comercial $comercial
    */
    class Pedido extends \yii\db\ActiveRecord {

        /**
         * {@inheritdoc}
        */
        public static function tableName() {
            return 'pedido';
        }

        /**
         * {@inheritdoc}
        */
        public function rules() {
            return [
                [['total', 'id_cliente', 'id_comercial'], 'required'],
                [['total'], 'number'],
                [['fecha'], 'safe'],
                [['id_cliente', 'id_comercial'], 'integer'],
                [['id_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::class, 'targetAttribute' => ['id_cliente' => 'id']],
                [['id_comercial'], 'exist', 'skipOnError' => true, 'targetClass' => Comercial::class, 'targetAttribute' => ['id_comercial' => 'id']],
            ];
        }

        /**
         * {@inheritdoc}
        */
        public function attributeLabels() {
            return [
                'id' => 'ID',
                'total' => 'Total',
                'fecha' => 'Fecha',
                'id_cliente' => 'Id Cliente',
                'id_comercial' => 'Id Comercial',
            ];
        }

        /**
         * Gets query for [[Cliente]].
         *
         * @return \yii\db\ActiveQuery
        */
        public function getCliente() {
            return $this->hasOne(Cliente::class, ['id' => 'id_cliente']);
        }

        /**
         * Gets query for [[Comercial]].
         *
         * @return \yii\db\ActiveQuery
        */
        public function getComercial() {
            return $this->hasOne(Comercial::class, ['id' => 'id_comercial']);
        }

        /**
         * Array de objetos donde estas todos los comerciales
         *
         * @return array
        */
        public function todosComerciales(): array  {
            // Array de objetos donde estas todos los comerciales
            $comerciales = Comercial::find()->all();

            // Array asociativo donde el indice es el id del comercial y el valor es el nombre
            return  ArrayHelper::map($comerciales,'id', 'nombreCompleto');
        }

        /**
         * Obtiene todos los clientes y sus nombres.
         *
         * @return array la lista de clientes con sus nombres como pares clave-valor
        */
        public static function todosClientes(): array {
            $clientes = Cliente::find()->all();

            return  ArrayHelper::map($clientes,'id','nombreCompleto');
        }
    }
